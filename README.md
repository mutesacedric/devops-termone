# Math Operations API

This project is a simple RESTful API for performing basic math operations: addition, subtraction, multiplication, and division.

## Project Structure

The project is structured as follows:

- `src/main/java`: This directory contains the main source code for the API.
- `src/test/java`: This directory contains the unit, integration, and end-to-end tests for the API.

## API Endpoints

The API has one main endpoint:

- `POST /api/v1/doMath`: Perform a math operation. The request body should be a JSON object with `operand1`, `operand2`, and `operation` fields. The `operation` field should be one of `+`, `-`, `*`, or `/`.

## Testing

The tests for the API are located in the `src/test/java` directory. There are three types of tests:

- Unit tests: These tests are located in the `src/test/java/unit` directory. They test individual components of the API in isolation.
- Integration tests: These tests are located in the `src/test/java/integration` directory. They test the interaction between multiple components of the API.
- End-to-end tests: These tests are located in the `src/test/java/e2e` directory. They test the entire API from start to finish, simulating real user requests.

To run the tests, you can use the following command in your terminal:

```bash
mvn test