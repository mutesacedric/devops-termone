package rca.devopsexam.v1.unit;

import org.junit.jupiter.api.Test;
import rca.devopsexam.v1.exceptions.InvalidOperationException;
import rca.devopsexam.v1.implementations.MathOperatorImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MathOperatorImplTest {
    @Test
    public void testDoMathAddition() throws InvalidOperationException {
        MathOperatorImpl operator = new MathOperatorImpl();
        double result = operator.doMath(2, 3, "+");
        assertEquals(5, result);
    }

    @Test
    public void testDoMathSubtraction() throws InvalidOperationException {
        MathOperatorImpl operator = new MathOperatorImpl();
        double result = operator.doMath(5, 3, "-");
        assertEquals(2, result);
    }

    @Test
    public void testDoMathMultiplication() throws InvalidOperationException {
        MathOperatorImpl operator = new MathOperatorImpl();
        double result = operator.doMath(2, 3, "*");
        assertEquals(6, result);
    }

    @Test
    public void testDoMathDivision() throws InvalidOperationException {
        MathOperatorImpl operator = new MathOperatorImpl();
        double result = operator.doMath(6, 3, "/");
        assertEquals(2, result);
    }

    @Test
    public void testDoMathDivisionByZero() {
        MathOperatorImpl operator = new MathOperatorImpl();
        assertThrows(InvalidOperationException.class, () -> operator.doMath(6, 0, "/"));
    }

    @Test
    public void testDoMathUnknownOperation() {
        MathOperatorImpl operator = new MathOperatorImpl();
        assertThrows(InvalidOperationException.class, () -> operator.doMath(6, 3, "^"));
    }
}