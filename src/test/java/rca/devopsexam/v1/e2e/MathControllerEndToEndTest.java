package rca.devopsexam.v1.e2e;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
public class MathControllerEndToEndTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testDoMathAddition() throws Exception {
        mockMvc.perform(post("/api/v1/doMath")
                        .contentType("application/json")
                        .content("{\"operand1\":2,\"operand2\":3,\"operation\":\"+\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.calcResponse").value(5.0));
    }

    @Test
    public void testDoMathSubtraction() throws Exception {
        mockMvc.perform(post("/api/v1/doMath")
                        .contentType("application/json")
                        .content("{\"operand1\":5,\"operand2\":3,\"operation\":\"-\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.calcResponse").value(2.0));
    }

    @Test
    public void testDoMathMultiplication() throws Exception {
        mockMvc.perform(post("/api/v1/doMath")
                        .contentType("application/json")
                        .content("{\"operand1\":2,\"operand2\":3,\"operation\":\"*\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.calcResponse").value(6.0));
    }

    @Test
    public void testDoMathDivision() throws Exception {
        mockMvc.perform(post("/api/v1/doMath")
                        .contentType("application/json")
                        .content("{\"operand1\":6,\"operand2\":3,\"operation\":\"/\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.calcResponse").value(2.0));
    }

    @Test
    public void testDoMathDivisionByZero() throws Exception {
        mockMvc.perform(post("/api/v1/doMath")
                        .contentType("application/json")
                        .content("{\"operand1\":2,\"operand2\":0,\"operation\":\"/\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDoMathUnknownOperation() throws Exception {
        mockMvc.perform(post("/api/v1/doMath")
                        .contentType("application/json")
                        .content("{\"operand1\":2,\"operand2\":3,\"operation\":\"^\"}"))
                .andExpect(status().isBadRequest());
    }
}