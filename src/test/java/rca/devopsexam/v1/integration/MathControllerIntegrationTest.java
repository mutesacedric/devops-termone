package rca.devopsexam.v1.integration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import rca.devopsexam.v1.controllers.MathController;
import rca.devopsexam.v1.dtos.DoMathRequest;
import rca.devopsexam.v1.exceptions.InvalidOperationException;
import rca.devopsexam.v1.implementations.MathOperatorImpl;
import rca.devopsexam.v1.payload.ApiResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@SpringBootTest
public class MathControllerIntegrationTest {
    @Autowired
    private MathController controller;

    @MockBean
    private MathOperatorImpl operator;

    @Test
    public void testDoMathAddition() throws Exception {
        when(operator.doMath(anyDouble(), anyDouble(), eq("+"))).thenReturn(5.0);
        ApiResponse response = controller.doMath(new DoMathRequest(2, 3, "+")).getBody();
        assert response != null;
        assertEquals(5.0, response.getCalcResponse());
        assertEquals(true, response.getSuccess());
    }

    @Test
    public void testDoMathSubtraction() throws Exception {
        when(operator.doMath(anyDouble(), anyDouble(), eq("-"))).thenReturn(-1.0);
        ApiResponse response = controller.doMath(new DoMathRequest(2, 3, "-")).getBody();
        assert response != null;
        assertEquals(-1.0, response.getCalcResponse());
        assertEquals(true, response.getSuccess());
    }

    @Test
    public void testDoMathMultiplication() throws Exception {
        when(operator.doMath(anyDouble(), anyDouble(), eq("*"))).thenReturn(6.0);
        ApiResponse response = controller.doMath(new DoMathRequest(2, 3, "*")).getBody();
        assert response != null;
        assertEquals(6.0, response.getCalcResponse());
        assertEquals(true, response.getSuccess());
    }

    @Test
    public void testDoMathDivision() throws Exception {
        when(operator.doMath(anyDouble(), anyDouble(), eq("/"))).thenReturn(0.6666666666666666);
        ApiResponse response = controller.doMath(new DoMathRequest(2, 3, "/")).getBody();
        assert response != null;
        assertEquals(0.6666666666666666, response.getCalcResponse());
        assertEquals(true, response.getSuccess());
    }

    @Test
    public void testDoMathDivisionByZero() throws InvalidOperationException {
        when(operator.doMath(anyDouble(), eq(0.0), eq("/"))).thenThrow(new InvalidOperationException("Cannot divide by 0"));
        Exception exception = assertThrows(InvalidOperationException.class, () -> {
            controller.doMath(new DoMathRequest(2, 0, "/"));
        });
        assertEquals("Cannot divide by 0", exception.getMessage());
    }

    @Test
    public void testDoMathUnknownOperation() throws InvalidOperationException {
        when(operator.doMath(anyDouble(), anyDouble(), eq("^"))).thenThrow(new RuntimeException("Unknown operation"));
        Exception exception = assertThrows(RuntimeException.class, () -> {
            controller.doMath(new DoMathRequest(2, 3, "^"));
        });
        assertEquals("Unknown operation", exception.getMessage());
    }
}