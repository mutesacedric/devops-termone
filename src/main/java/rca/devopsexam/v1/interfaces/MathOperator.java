package rca.devopsexam.v1.interfaces;

import rca.devopsexam.v1.exceptions.InvalidOperationException;

public interface MathOperator {
    double doMath(double operand1, double operand2, String operation) throws InvalidOperationException;
}
