package rca.devopsexam.v1.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiResponse {
    private Boolean success;
    private String message;
    private Double calcResponse;

    public ApiResponse(Boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public ApiResponse(Boolean success, Double calcResponse) {
        this.success = success;
        this.calcResponse = calcResponse;
    }

    public static ApiResponse success(Double calcResponse) {
        return new ApiResponse(true, calcResponse);
    }

    public static ApiResponse fail(String message) {
        return new ApiResponse(false, message);
    }
}